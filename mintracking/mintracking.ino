#include "SDP.h"
#include "Wire.h"
#include <Servo.h>

#define HistorySize 2
#define ExhaleThreshold 20
#define InhaleThreshold -1 //0
#define ExhaleToMaxRatio .7
#define MaxPot 1024
#define Idle 10
#define DelayToTrackMin 4 //How many loops to wait for valve to open
#define DelayToTrackMax 8 
//#define OpenedServoAngle 89 //for string valve
#define OpenedServoAngle 140 //for butterfly valce
//Don't exceed this or you will break servo arm
//#define ClosedServoAngle 106 // string valve
#define ClosedServoAngle 180//180 //for butterfly valve 


SDP_Controller SDP800; //pressure sensor
Servo myservo;  // create servo object to control a servo

int pwmPin = 11;
int servoPin = 9;
int potPin = 0;

double potVal,servoAngle;
double currentDP, pwmVal, minPressure,maxPressure;
double DPHistory[HistorySize];
unsigned int circIdx, delayCounter;

enum BreathingStates {OPENVALVE,
		      CLOSEVALVE,
		      OPENINGVALVE,
		      CLOSINGVALVE};
enum BreathingStates currentState;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(pwmPin,OUTPUT);
  //https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM
  //Needed to use Timer Control 2 (pins 11 and 3) since the servo library disables pwm for
  // Timer control 1 (p202 of manual)
  //set prescale divider for timer 2 to 256 (110)
  TCCR2B |= _BV(CS22)|_BV(CS21);TCCR2B &= ~_BV(CS20);

  myservo.attach(servoPin);
  servoAngle = OpenedServoAngle;
  myservo.write(servoAngle);

  Wire.begin();
  SDP800.begin();
  SDP800.startContinuousMeasurement(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_AVERAGING_TILL_READ);
  delay(1000);
  pwmVal = Idle; //start with slow motor
  //memset(DPHistory, 0, HistorySize * sizeof(double));
  circIdx = 0;
  delayCounter = DelayToTrackMin;
  currentState = OPENINGVALVE;
}

//void loop() {}
void loop(){
  potVal = analogRead(potPin);
  pwmVal = potVal/4;
  currentDP = SDP800.getDiffPressure();


  switch (currentState){
  case OPENINGVALVE:
   // Serial.println("OPENINGVALVE");
    // wait few samples before tracking minimum
    // also waiting in open is useful in case of spurious pressures
    if (delayCounter == 0) {
      minPressure = currentDP;
      currentState = OPENVALVE;
    } else {
      --delayCounter;
    }
    break;

  case OPENVALVE: //inhaling
   // Serial.println("OPENVALVE");
    if(currentDP < minPressure) {
      minPressure = currentDP;
//    } else if((currentDP - minPressure) > ExhaleThreshold){
    } else if((currentDP - minPressure) > max(min(maxPressure*.5,ExhaleThreshold),5)){      
      //Close valve
      servoAngle = ClosedServoAngle;
      currentState = CLOSINGVALVE;
      delayCounter = DelayToTrackMax;
    }
    break;
    
  case CLOSINGVALVE:
   // Serial.println("OPENINGVALVE");
    // wait few samples before tracking maximum
    if (delayCounter == 0) {
      maxPressure = currentDP;
      currentState = CLOSEVALVE;
    } else {
      --delayCounter;
    }

    //if under threshold
    //if(false){
      if((currentDP < InhaleThreshold) &&
	     (currentDP < DPHistory[circIdx])) {
	 // open Valve: pressure would go up
	 servoAngle = OpenedServoAngle;
	 delayCounter = DelayToTrackMin;
	 currentState = OPENINGVALVE;
       } 
    break;
    

  case CLOSEVALVE: //exhaling
   // Serial.println("CLOSEVALVE");
    if(currentDP > maxPressure) {
      maxPressure = currentDP;
    } 

    else if((true&&(maxPressure - currentDP >
	     maxPressure*ExhaleToMaxRatio))||
	    //SafeGuard: if under threshold and going down
	    ((currentDP < InhaleThreshold) &&
	     (currentDP < DPHistory[circIdx]))) {
      // open Valve: pressure would go up
      servoAngle = OpenedServoAngle;
      delayCounter = DelayToTrackMin;
      currentState = OPENINGVALVE;
    }
    break;

  }//switch

  DPHistory[circIdx] = currentDP;
  circIdx = (circIdx + 1) % HistorySize;

  myservo.write(servoAngle);
  analogWrite(pwmPin,pwmVal);
  //Serial.print(pwmVal/10);
  //Serial.print(' ');
  //Serial.print(maxPressure);
  //Serial.print(' ');
  Serial.print(minPressure+maxPressure);
  Serial.print(' ');
  Serial.print(servoAngle/10);
  Serial.print(' ');
  Serial.println(currentDP);
  delay(40);
  }

//TODO track period, open valve at a 95% period, track maximum to determine drift 
//Find static pressure baseline: 
//1. map pwm to press(not too robust since this could change) could use calibration
//2. determine size of jump due to valve switching (interpolate by adding delta previous samples to jump)
//3. Add max to min should give you close to number if you assume we breather around 0 mean, which is not true
 
