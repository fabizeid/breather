#include "SDP.h"
#include "Wire.h"
#include <Servo.h>
#define HistorySize 2
#define ExhaleThreshold 20
#define InhaleThreshold -1 //0
//#define ExhaleToMaxRatio 0.8//.2//.7
#define MaxPot 1024
#define Idle 10
#define DelayToTrackMin 4 //How many loops to wait for valve to open
#define DelayToTrackMax 6//8 
#define FastBreathingThreshold 7
//#define OpenedServoAngle 89 // for string valve
#define OpenedServoAngle 140 //for butterfly valve
//Don't exceed this or you will break servo arm
//#define ClosedServoAngle 106 // string valve
#define ClosedServoAngle 180//180 //for butterfly valve 
#define ReducedServoAngle 160

SDP_Controller SDP800; //pressure sensor
Servo myservo;  // create servo object to control a servo

int pwmPin = 11;
int servoPin = 9;
int potPin = 0;

double potVal,servoAngle,delayMs;
double currentDP, pwmVal, minPressure,maxPressure;
double DPHistory[HistorySize];
unsigned int circIdx, delayCounter, inhaleCounter,exhaleCounter, counter;

enum BreathingStates {OPENVALVE,
                      CLOSEVALVE,
                      OPENINGVALVE,
                      CLOSINGVALVE};
enum BreathingStates currentState;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(pwmPin,OUTPUT);
  //https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM
  //Needed to use Timer Control 2 (pins 11 and 3) since the servo library disables pwm for
  // Timer control 1 (p202 of manual)
  //set prescale divider for timer 2 to 256 (110)
  TCCR2B |= _BV(CS22)|_BV(CS21);TCCR2B &= ~_BV(CS20);

  myservo.attach(servoPin);
  servoAngle = OpenedServoAngle;
  myservo.write(servoAngle);

  Wire.begin();
  SDP800.begin();
  SDP800.startContinuousMeasurement(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_AVERAGING_TILL_READ);
  delay(1000);
  pwmVal = Idle; //start with slow motor
  //memset(DPHistory, 0, HistorySize * sizeof(double));
  circIdx = 0;
  delayCounter = DelayToTrackMin;
  currentState = OPENINGVALVE;
  delayMs = 40;  
  counter = 0;
}

//void loop() {}
void loop(){
  potVal = analogRead(potPin);
  pwmVal = potVal/4;
  currentDP = SDP800.getDiffPressure();
  counter++;

  switch (currentState){
  case OPENINGVALVE:
   // Serial.println("OPENINGVALVE");
    // wait few samples before tracking minimum
    // also waiting in open is useful in case of spurious pressures
    
    if (delayCounter == 0) {
      minPressure = currentDP;
      currentState = OPENVALVE;
    } else {
      --delayCounter;
    }
    break;

  case OPENVALVE: //inhaling
   // Serial.println("OPENVALVE");

    if(currentDP < minPressure) {
      minPressure = currentDP;
      //first part of next statement was added since during deep and heavy breathing we get some local maximums while still inhaling
      // this was confusing the algorithm into thinking we started exhaling
    } else if(currentDP > maxPressure*(ExhaleThreshold/120) && //only consider next statement when breathing goes above a threshold
	      (currentDP - minPressure) > max(min(maxPressure*.5,ExhaleThreshold),5)){
      //Close valve
      
      inhaleCounter = counter;
      counter = 0;
      if (inhaleCounter < FastBreathingThreshold){
        //Skip CLOSINGVALVE state, assume breathing very fast
        currentState = CLOSEVALVE;
        maxPressure = currentDP;   
        servoAngle = ReducedServoAngle;     
      } else {
        currentState = CLOSINGVALVE;
        delayCounter = DelayToTrackMax;
        servoAngle = ClosedServoAngle;
      }

    }
    break;
    
  case CLOSINGVALVE:
   // Serial.println("OPENINGVALVE");
    // wait few samples before tracking maximum
    if (delayCounter == 0) {
      maxPressure = currentDP;
      currentState = CLOSEVALVE;
    } else {
      --delayCounter;
    }
    if((currentDP < InhaleThreshold) &&
       (currentDP < DPHistory[circIdx])) {
         // open Valve: pressure would go up
         servoAngle = OpenedServoAngle;
         delayCounter = DelayToTrackMin;
         exhaleCounter = counter;
         counter = 0;
         currentState = OPENINGVALVE;
       } 

    break;
    

  case CLOSEVALVE: //exhaling
   // Serial.println("CLOSEVALVE");
    if(currentDP > maxPressure) {
      maxPressure = currentDP;
    } 

    else if((true&&(maxPressure - currentDP >
             maxPressure*getExhaletoMaxRatio(inhaleCounter,maxPressure)))||
            //SafeGuard: if under threshold and going down
            ((currentDP < InhaleThreshold) &&
             (currentDP < DPHistory[circIdx]))) {
      // open Valve: pressure would go up
      servoAngle = OpenedServoAngle;
      exhaleCounter = counter;
      counter = 0;
      if (exhaleCounter < 7){
        //Skip OPENINGVALVE state, assume breathing very fast
        currentState = OPENVALVE;
        minPressure = currentDP;        
      } else {
        currentState = OPENINGVALVE;
        delayCounter = DelayToTrackMin;
      }

    }
    break;

  }//switch

  DPHistory[circIdx] = currentDP;
  circIdx = (circIdx + 1) % HistorySize;

  myservo.write(servoAngle);
  analogWrite(pwmPin,pwmVal);
  //Serial.print(pwmVal/10);
  //Serial.print(' ');
  //Serial.print(getExhaletoMaxRatio(inhaleCounter)*100);
  //Serial.print(' ');
  Serial.print(counter);
  Serial.print(' ');
  Serial.print(currentState*10);
  Serial.print(' ');
  Serial.print(servoAngle-120);
  Serial.print(' ');
  Serial.println(currentDP);
  delay(delayMs);
  }

//TODO track period, open valve at a 95% period, track maximum to determine drift 
//Find static pressure baseline: 
//1. map pwm to press(not too robust since this could change) could use calibration
//2. determine size of jump due to valve switching (interpolate by adding delta previous samples to jump)
//  -already did this but extrapolation was not accurate
//3. Add max to min should give you close to number if you assume we breather around 0 mean (which is not true)
//4. when breathing hard and slow, we should still open the valve earlier not only when we are breathing fast. It's all about the slope.


//If exhale is 40*40 ms long  set exhaleToMaxRatio .8 if exhale is 6 set exhaleToMaxRatio 0;
//stay most of the time around .7: formula = (1-(1./(t-5)))*.8) where t = 6:40
//Consider sigmoid y = 1/(1+exp(x)): (.6./(1+exp(-(x-20)))+.1) => sigmoid from .1 to .7
double gettExhaletoMaxRatio(int delay) {
  // The faster breathing frequency, the earlier we should open the valve. Also the higher the maxPressure the earlier we should open the valve. The valve time to open depends on the slope of inhale but by the time we calculate it, it's too late.
  //Or I could use the decaying average of the previous slopes (which I can't compute easily since valve is opening during transition).
  // Also if negative pressure is too high from previous inhales then we need to open earlier
  // Need to react to deep slow breathing and fast shallow breathing
  //double 
if (delay < 7)
 return .1;
 //else return .3; 
 //     delay = 7;
  else return (1-(1/((double)delay-5)))*.8;
}

double getExhaletoMaxRatio(double delay, double maxPressure) {
  // breathing freq based ratio: open valve earlier for fast and shallow breaths
  // fast .1% off peak, slow .75% off peak
  double ratio = .65/(1+exp(-(delay-10)))+.1;
  // breathing amplitude based ratio only when not in fast breathing mode.
  // In regular breathing mode the closingvalve state will allow pressure to settle
  // hence giving us a more relevant maxPressure.
  if (delay > FastBreathingThreshold) {
    //20 pascal 1, 80 = .2 transition around 40
    // ratio = min(ratio,1-.8/(1+exp(-(maxPressure-40)/2)));
   ratio = min(ratio,1-.85/(1+exp(-(maxPressure-40)/2)));
  }
  // ratio = min(freqRatio,ampRatio);
  return ratio;
}


//Variable to take into consideration: breathing rate, exhale Max, inhale min after valve closed, inhale min during valve closed, pwm of motor, check valves
// Future think about teckniques to equalize breathing signal by reducing valve switching effects in order to detect breathing deflection point. Also this would help in locking to deflection point and adjusting for drifts between breathing cycles

