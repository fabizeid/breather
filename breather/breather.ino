#include "SDP.h"
#include "Wire.h"
#include <Servo.h>
#define HistorySize 2
#define ExhaleThreshold 20
#define InhaleThreshold -1 //0
#define MaxPower 255
#define MaxPot 1024
#define Idle 10
#define MaxTargetPressure 60
#define MinTargetPressure 15
#define DesiredMinPressure -30
#define DelayToTrackMin 4 //How many loops to wait for valve to open
#define DelayToTrackMax 6 
#define FastBreathingThreshold 7
//#define OpenedServoAngle 89 //for string valve
#define OpenedServoAngle 140 //for butterfly valve
//Don't exceed this or you will break servo arm
//#define ClosedServoAngle 106 // string valve
#define ClosedServoAngle 180 //for butterfly valve 
#define ReducedServoAngle 160
#define BufferAngle 5
#include "PotUI.h"

SDP_Controller SDP800; //pressure sensor
Servo myservo;  // create servo object to control a servo


int pwmPin = 11;
PotUI ui(pwmPin);
int servoPin = 9;
int potPin = 0;

double potVal,servoAngle,delayMs,minServoAngle,minAngleError;;
double currentDP, pwmVal, minPressure,maxPressure,minPressureErr;
double DPHistory[HistorySize];
unsigned int circIdx, delayCounter,inhaleCounter,exhaleCounter, counter;

unsigned long now, lastTime, timeChange;

// pid vars

double targetDP,error,lastError,errorD,Kp,Kd,Kpv,Kdv,KpBlower,KpTargetDP;

enum BreathingStates {OPENVALVE,
                      CLOSEVALVE,
                      OPENINGVALVE,
                      CLOSINGVALVE};
enum BreathingStates currentState;

enum OperationModes currentMode,lastCurrentMode;



void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(pwmPin,OUTPUT);
  //https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM
  //Needed to use Timer Control 2 (pins 11 and 3) since the servo library disables pwm for
  // Timer control 1 (p202 of manual)
  //set prescale divider for timer 2 to 256 (110)
  TCCR2B |= _BV(CS22)|_BV(CS21);TCCR2B &= ~_BV(CS20);

  myservo.attach(servoPin);
  servoAngle = OpenedServoAngle;
  myservo.write(servoAngle);
  Wire.begin();
  SDP800.begin();
  SDP800.startContinuousMeasurement(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_AVERAGING_TILL_READ);
  delay(1000);
  pwmVal = analogRead(potPin)/4;
  //memset(DPHistory, 0, HistorySize * sizeof(double));
  circIdx = 0;
  delayCounter = DelayToTrackMin;
  currentState = OPENINGVALVE;
  counter = 0;
  targetDP = 15; //Pascal
  lastTime = 0;
  minServoAngle = ClosedServoAngle; 
  minPressure = 100; //Big number
  currentDP = 0;
  lastCurrentMode = FASTPID;

  //pid setup
  lastError = 0;
  Kp = .88;//256;//30;
  Kp = 2; //min tracking
  Kd = 6;//600;
  Kpv = .07;
  Kdv = 0.26;
  KpBlower = .3;
  KpTargetDP = .3;
  
}

//void loop() {}
void loop(){
  potVal = analogRead(potPin);
  currentMode = ui.track(potVal); 
  currentDP = SDP800.getDiffPressure();

  if (currentMode == SLOWPID || currentMode == FASTPIDPOT){
    targetDP = potVal/15;
  } else if (currentMode == NOPID || currentMode == NOVALVE){ 
    pwmVal = potVal/4;    
  }

  /*else {// currentMode == FASTPID
    // only set targetDP when we just switched
    if (lastCurrentMode != FASTPID)
      targetDP = 15;
  }*/
  lastCurrentMode = currentMode; 
  counter++;   
  if(currentMode == NOPID || currentMode == SLOWPID) {
  delayMs = 40;  
  switch (currentState){
  case OPENINGVALVE:
   // Serial.println("OPENINGVALVE");
    // wait few samples before tracking minimum
    // also waiting in open is useful in case of spurious pressures
    if (delayCounter == 0) {
      minPressure = currentDP;
      currentState = OPENVALVE;
    } else {
      --delayCounter;
    }
    break;

  case OPENVALVE: //inhaling
   // Serial.println("OPENVALVE");
    if(currentDP < minPressure) {
      minPressure = currentDP;
      //first part of next statement was added since during deep and heavy breathing we get some local maximums while still inhaling
      // this was confusing the algorithm into thinking we started exhaling
    } else if(currentDP > maxPressure*(ExhaleThreshold/120) && //only consider next statement when breathing goes above a threshold
	      (currentDP - minPressure) > max(min(maxPressure*.5,ExhaleThreshold),5)){
   /* } else if(currentDP > pwmVal*90/255) {*/
      if (currentMode == SLOWPID){
        //Adjust blower pwm based on minPressure
                  pwmVal = pidPwm();
      }

      inhaleCounter = counter;
      counter = 0;
      //Close valve
      if (inhaleCounter < FastBreathingThreshold){
        //Skip CLOSINGVALVE state, assume breathing very fast
        currentState = CLOSEVALVE;
        maxPressure = currentDP;   
        servoAngle = ReducedServoAngle;     
      } else {
        currentState = CLOSINGVALVE;
        delayCounter = DelayToTrackMax;
        servoAngle = ClosedServoAngle;
      }
    }
    break;
    
  case CLOSINGVALVE:
   // Serial.println("OPENINGVALVE");
    // wait few samples before tracking maximum
    if (delayCounter == 0) {
      maxPressure = currentDP;
      currentState = CLOSEVALVE;
    } else {
      --delayCounter;
    }

    if((currentDP < InhaleThreshold) &&
       (currentDP < DPHistory[circIdx])) {
         // open Valve: pressure would go up
         servoAngle = OpenedServoAngle;
         delayCounter = DelayToTrackMin;
         exhaleCounter = counter;
         counter = 0;
         currentState = OPENINGVALVE;
       } 
    break;
    

  case CLOSEVALVE: //exhaling
    // Serial.println("CLOSEVALVE");
    if(currentDP > maxPressure) {
      maxPressure = currentDP;
    } 

    else if((true&&(maxPressure - currentDP >
             maxPressure*getExhaletoMaxRatio(inhaleCounter,maxPressure)))||
            //SafeGuard: if under threshold and going down
            ((currentDP < InhaleThreshold) &&
             (currentDP < DPHistory[circIdx]))) {
      // open Valve: pressure would go up
      servoAngle = OpenedServoAngle;
      exhaleCounter = counter;
      counter = 0;
      if (exhaleCounter < 7){
        //Skip OPENINGVALVE state, assume breathing very fast
        currentState = OPENVALVE;
        minPressure = currentDP;        
      } else {
        currentState = OPENINGVALVE;
        delayCounter = DelayToTrackMin;
      }
    }
    break;

  }//switch

  DPHistory[circIdx] = currentDP;
  circIdx = (circIdx + 1) % HistorySize;
  } else if(currentMode == FASTPID || currentMode == FASTPIDPOT){ //fastpid
    delayMs = 9;
    if (counter > 3000/delayMs){
         counter = 0;
	 //decrease if needed
	 decreasePwm();
	 if(currentMode == FASTPID){
	   //adjust targetDP if needed
	   adjustTargetDP();
	 }
	 //reset min tracking every 3 seconds
	 minServoAngle = ClosedServoAngle;
	 minPressure = currentDP;	 
    }
    if(currentDP < minPressure) {
     minPressure = currentDP;
   }
 
    pidAngle(currentDP);
  } else {
    servoAngle = OpenedServoAngle;
    delayMs = 40;
  }
  myservo.write(servoAngle);
  analogWrite(pwmPin,pwmVal);
  
  //Serial.print(currentMode*20);
  //Serial.print(' ');
  //Serial.print(pwmVal/10);
  //Serial.print(' ');
  //Serial.print(counter);
  //Serial.print(' ');
  //Serial.print(currentState*10);
  //Serial.print(' ');
  Serial.print(servoAngle-120);  
  Serial.print(' ');
  Serial.print(potVal/100);
  Serial.print(' ');
  //Serial.print(timeChange);
  //Serial.print(' ');
  Serial.print(targetDP);
  Serial.print(' ');
  Serial.println(currentDP);
  delay(delayMs);
  }

double pidPwm(void) {

  error = targetDP - minPressure;
  errorD = error - lastError;
  lastError = error;
  
 // pwmVal = pwmVal + error*Kp + errorD*Kd;
  pwmVal = pwmVal + error*Kp;
  if(pwmVal > MaxPower)
    pwmVal = MaxPower;
  else if (pwmVal < Idle)
    pwmVal = Idle;
    
 return pwmVal; 
}

double getExhaletoMaxRatio(double delay, double maxPressure) {
  // breathing freq based ratio: open valve earlier for fast and shallow breaths
  // fast .1% off peak, slow .75% off peak
  double ratio = .65/(1+exp(-(delay-10)))+.1;
  // breathing amplitude based ratio only when not in fast breathing mode.
  // In regular breathing mode the closingvalve state will allow pressure to settle
  // hence giving us a more relevant maxPressure.
  if (delay > FastBreathingThreshold) {
    //20 pascal 1, 80 = .2 transition around 40
    // ratio = min(ratio,1-.8/(1+exp(-(maxPressure-40)/2)));
   ratio = min(ratio,1-.5/(1+exp(-(maxPressure-40)/2)));
  }
  // ratio = min(freqRatio,ampRatio);
  return ratio;
}

/* double getExhaletoMaxRatio(int delay) { */
/*  if (delay < 7) */
/*  return .1; */
/*   else return (1-(1/((double)delay-5)))*.8; */
/* } */




void adjustTargetDP(void){
  // minPressure if below -30 increase targetDP
  minPressureErr = DesiredMinPressure - minPressure;
  targetDP = targetDP + minPressureErr * KpTargetDP;
  if(targetDP > MaxTargetPressure)
    targetDP = MaxTargetPressure;
  else if (targetDP < MinTargetPressure)
    targetDP = MinTargetPressure;    
}

//Only called when valve is completely open
// and can't keep up
void increasePwm(void) {

  //already calculated in pidAngle
  //error = targetDP - minPressure;
  //errorD = error - lastError;
  //lastError = error;
  
 // pwmVal = pwmVal + error*Kp + errorD*Kd;
  pwmVal = pwmVal + error*KpBlower;
  if(pwmVal > MaxPower)
    pwmVal = MaxPower;
  else if (pwmVal < Idle)
    pwmVal = Idle; 
}

void decreasePwm(void) {
  //desiredMinAngle = OpenedServoAngle + BufferAngle;
  minAngleError = minServoAngle - OpenedServoAngle; 
  if(minAngleError > BufferAngle) {
    // reducePWM
   pwmVal = pwmVal - minAngleError*KpBlower*10;
  
  if(pwmVal > MaxPower)
    pwmVal = MaxPower;
  else if (pwmVal < Idle)
    pwmVal = Idle; 
  }
}

void pidAngle(double currentDP) {

  now = millis();
  timeChange = (now - lastTime);
  lastTime = now;

  
  error = targetDP - currentDP;
  // Use currentDP instead error to avoid derivative kicks
  errorD = currentDP - lastError;
  lastError = currentDP;
  //if(error <5 && error > 5 ) return;
  //servoAngle = servoAngle - error*Kpv - errorD*Kdv;
  servoAngle = servoAngle - error*Kpv + errorD*Kdv;
  if(servoAngle < minServoAngle){
    minServoAngle = servoAngle; 
  }
  if(servoAngle > ClosedServoAngle)
    servoAngle = ClosedServoAngle;
  else if (servoAngle < OpenedServoAngle){
   increasePwm();
   servoAngle = OpenedServoAngle;
  }
}


