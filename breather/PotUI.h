#ifndef PotUI_H
#define PotUi_H
#define PotThreshold 5
enum OperationModes { NOPID,
		      SLOWPID,
		      FASTPID,
		      FASTPIDPOT,
		      NOVALVE,
		      numOperationModes
};

class PotUI{
 public:

 PotUI(int pwmPinArg):currentMode(FASTPID),
    currentState(NORMAL),
    lastVal(0),lastTime(0),
    switchedDirection(false), pwmPin(pwmPinArg){};
  
  OperationModes getCurrentMode(void){
    return currentMode;
  };
  
  OperationModes track(int potValArg){

    now = millis();
    if(now - lastTime < 40) return currentMode;
    lastTime = now;
    
  
    int potVal = potValArg/10; //get rid of jitter
    switch(currentState){
    case NORMAL:
      if (potVal < PotThreshold ){
	numCrossed = 1;
      	switchedDirection = false;
	currentState = CROSSEDDOWN;
      }
      break;
    case CROSSEDDOWN:
      if(potVal > lastVal){
	switchedDirection = true;
	if (potVal > PotThreshold){
	  numCrossed++;
	  switchedDirection = false;
	  currentState = CROSSEDUP;
	}	
      } else if((potVal < lastVal) && switchedDirection){
	currentState = NORMAL;
      }      	      
      break;
    case CROSSEDUP:
      if(potVal < lastVal){
	switchedDirection = true;
	if (potVal < PotThreshold){
	  numCrossed++;
	  switchedDirection = false;
	  currentState = CROSSEDDOWN;
	}	
      } else if((potVal > lastVal) && switchedDirection){
	currentState = NORMAL;
      }
    }
    lastVal = potVal;
    if(numCrossed == 5){
      cycleOperationMode();
      analogWrite(pwmPin,MaxPower);
      delay(1000);
      analogWrite(pwmPin,0);
      delay(1000);
      currentState = NORMAL;
      numCrossed = 0;
    }
    return currentMode;
  }
  
 private:
  
  OperationModes currentMode;
  typedef enum { NORMAL,
		 CROSSEDDOWN,
		 CROSSEDUP,
		 SWITCHINGMODES} UIStates;

  UIStates currentState;
  int lastVal,numCrossed,pwmPin;
  unsigned long now, lastTime, timeChange;
  boolean switchedDirection;
  void cycleOperationMode(void){
    currentMode = (currentMode + 1) % (numOperationModes);
  }
};

#endif
