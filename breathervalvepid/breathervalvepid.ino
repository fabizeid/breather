#include "SDP.h"
#include "Wire.h"
#include <Servo.h>

#define MaxPower 255
#define MaxPot 1024
#define Idle 10
#define MaxTargetPressure 60
#define MinTargetPressure 15
#define DesiredMinPressure -30
#define OpenedServoAngle 140 //for butterfly valve
//Don't exceed this or you will break servo arm
#define ClosedServoAngle 180//180 //for butterfly valve
#define ReducedServoAngle 160
#define BufferAngle 5

SDP_Controller SDP800; //pressure sensor
Servo myservo;  // create servo object to control a servo

int pwmPin = 11; 
int servoPin = 9;
int potPin = 0; 
char receivedData,lastChar;
int loopcounter;
double potVal,servoAngle, delayMs,kScale,minServoAngle,minAngleError,minPressure,minPressureErr;
double currentDP, pwmVal,targetDP,error,lastError,errorD,Kp,Kd,lastKp,lastKd,KpBlower,KpTargetDP;
unsigned long now, lastTime, timeChange;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(pwmPin,OUTPUT);
  //Change Timer 1 clock (pins 9 (OC1A) and 10 (OC1B))
  //https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM
  //p 173 of manual: CS12 CS11 CS10: 011 /64, 100 /256, 101 /1024
  //set prescale divider for timer 1 to 1024 (101)
  //TCCR1B |= _BV(CS12)|_BV(CS10); TCCR1B &= ~_BV(CS11);
  
  //Needed to use Timer Control 2 (pins 11 and 3) since the servo library disables pwm for 
  // Timer control 1 (p202 of manual)
  //set prescale divider for timer 2 to 256 (110)
  TCCR2B |= _BV(CS22)|_BV(CS21);TCCR2B &= ~_BV(CS20);
  
  myservo.attach(servoPin);
  servoAngle = ReducedServoAngle;
  myservo.write(servoAngle);
 

  Wire.begin();
  SDP800.begin();
  SDP800.startContinuousMeasurement(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_AVERAGING_TILL_READ);
  delay(1000);
  targetDP = MinTargetPressure ; //Pascal
  loopcounter = 0;
  lastError = 0;
  pwmVal = Idle; 
  Kp = .07;//0.02;//.88;//256;//30;
  Kd = 0.26;//600;
  KpBlower = .3;//.88;
  KpTargetDP = .3;
  lastKp = Kp;
  lastKd = Kd;
  delayMs = 10;
  lastTime = 0;
  kScale = 1000;
  receivedData = '0';//'w';
  minServoAngle = ClosedServoAngle;
  minPressure = 100; //Big number
  currentDP = 0;
}

//void loop() {}
void loop(){
 
 if (loopcounter++ > 3000/delayMs){
   loopcounter = 0;
   //decrease if needed
   decreasePwm();
   //adjust targetDP if needed
   adjustTargetDP();
   //reset min tracking every 3 seconds
   minServoAngle = ClosedServoAngle;
   minPressure = currentDP;
 }

 
 potVal = analogRead(potPin);
 processCmds();

 
  //pwmVal = potVal/4;
  //pwmVal = 255;
  //targetDP = potVal/10;
  //Kd = potVal/1000;
  //Kp = potVal/10000;
 
  currentDP = SDP800.getDiffPressure();
  if(currentDP < minPressure) {
    minPressure = currentDP;
  }
  pidAngle();

  myservo.write(servoAngle);   
  analogWrite(pwmPin,pwmVal);
  if(loopcounter%10 == 0) {
  //Serial.print(timeChange);
  //Serial.print(' ');
  //Serial.print(error*Kp);
  //Serial.print(' ');
  //Serial.print(errorD*Kd);
  //Serial.print(' ');
  //Serial.print(Kp);
  //Serial.print(' ');
  //Serial.print(Kd);
  //Serial.print(' ');
  Serial.print(minAngleError);
  Serial.print(' ');
  Serial.print(pwmVal);
  Serial.print(' ');
  Serial.print(servoAngle - 100);
  Serial.print(' ');
  Serial.print(targetDP);
  Serial.print(' ');
  ////Serial.print(movingAvg/MASize);
  ////Serial.print(' ');
  ////Serial.print(errorD);
  ////Serial.print(' ');
  Serial.println(currentDP);
  
  
  }

  delay(delayMs);
  
}

void adjustTargetDP(void){
  // minPressure if below -30 increase targetDP
  minPressureErr = DesiredMinPressure - minPressure;
  targetDP = targetDP + minPressureErr * KpTargetDP;
  if(targetDP > MaxTargetPressure)
    targetDP = MaxTargetPressure;
  else if (targetDP < MinTargetPressure)
    targetDP = MinTargetPressure;    
}

//Only called when valve is completely open
// and can't keep up
void increasePwm(void) {

  //already calculated in pidAngle
  //error = targetDP - minPressure;
  //errorD = error - lastError;
  //lastError = error;
  
 // pwmVal = pwmVal + error*Kp + errorD*Kd;
  pwmVal = pwmVal + error*KpBlower;
  if(pwmVal > MaxPower)
    pwmVal = MaxPower;
  else if (pwmVal < Idle)
    pwmVal = Idle; 
}

void decreasePwm(void) {
  //desiredMinAngle = OpenedServoAngle + BufferAngle;
  minAngleError = minServoAngle - OpenedServoAngle; 
  if(minAngleError > BufferAngle) {
    // reducePWM
   pwmVal = pwmVal - minAngleError*KpBlower*10;
  
  if(pwmVal > MaxPower)
    pwmVal = MaxPower;
  else if (pwmVal < Idle)
    pwmVal = Idle; 
  }
}

void pidAngle() {

  
  error = targetDP - currentDP;
  // servoAngle = servoAngle - error*Kp + errorD*Kd;
  // Check that update rate is constant
  // 
  now = millis();
  timeChange = (now - lastTime);
  lastTime = now;
  // Use instead errorD = currentDP - lastDP to avoid derivative kicks
  // http://brettbeauregard.com/blog/2011/04/improving-the-beginner%E2%80%99s-pid-derivative-kick/
  // errorD = error - lastError;
  // lastError = error;

  errorD = currentDP - lastError;
  lastError = currentDP;
  
  //if(error <5 && error > 5 ) return;
  //servoAngle = servoAngle - error*Kp - errorD*Kd;
  servoAngle = servoAngle - error*Kp + errorD*Kd;
  if(servoAngle < minServoAngle){
    minServoAngle = servoAngle; 
  }
  if(servoAngle > ClosedServoAngle)
    servoAngle = ClosedServoAngle;
  else if (servoAngle < OpenedServoAngle){
    increasePwm();
    servoAngle = OpenedServoAngle;
  }
    
}

void processCmds(void){
 if (Serial.available() > 0) {
         // read the incoming byte:
         lastChar = receivedData;
         receivedData = Serial.read();
	 lastKp = Kp;
	 lastKd = Kd;
 }

 switch (receivedData){
  case 'p':
    Kp = lastKp + (potVal - MaxPot/2)/kScale;
   break;
  case 'd':
    Kd = lastKd + (potVal - MaxPot/2)/kScale;
   break;
  case 'w':
   pwmVal = potVal/4;
   break;
  case 't':
   targetDP = potVal/10;
  break; 
  case '+':
   kScale /= 10;
   receivedData = lastChar;
   break;
  case '-':
   kScale *= 10;
   receivedData = lastChar;
   break;
 }
  

}

//TODO: make kp and kd a function of breathing rate and strength. That is increase kp when breathing fast and heavy
//low pass filter + fft to find breathing rate.

