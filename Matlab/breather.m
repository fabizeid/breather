function breather
%data = load('fastbreathing.log');
% dt = diff(data(:,3));
% d = [0;dt];
% dd = [0;0;diff(dt)];
% plot([data(:,[2 3]) d dd],'+-');
%data = load('slowbreathing.log');
% dt = diff(data(:,3));
% d = [0;dt];
% dd = [0;0;diff(dt)];
% figure
% plot([data(:,[2 3]) d dd],'+-');
data = load('fastslownobreathing.log');
pressure = data(:,3);
dt = diff(pressure);
d = [0;dt];
dd = [0;0;diff(dt)];
threshold = 65;
flags = calcflags(pressure,d,threshold);
%plot([data(:,[2 3]) d dd],'+-');
plot([data(:,[2 3]) d dd],'.-');
hold on
plot(flags);
plot(size(flags),threshold*[1 1]);
hold off
% Some Ideas
% Take into account change in motor speed or valve angle when calculating
% derivatives (subtract or add proportional value). Or at least ignore
% Ignore derivatives when toggling valve
%
% minimum of second derivative will give you when you start inhaling (it
% when the change of downward slope is maximum
% 
% Other ideas, use crossing of threshold which depends on exhale vs inhale
% and pwm and valve. 
% Try to find pressure due to pwm and valve without breathing. Should be
% somewhere close to the middle between the maximum and minimum. Keep
% adjusting after every breath.
% when pwm changes reset and recalculate. That target pressure could help
% us determing exhale point
% 
% open valve when value goes 
% 1. below 10 and derivative is negative and below a certain threshold (ie
% was on the way down with a certain slope) or
% 2. below 0 whichever is detected first (this would happen if we missed
% condition 1

% Need more data for slow breathing (make also switching longer)

% if history of last three derivative > 0 then up
% 
% in loop
% if currentD > 0 count++
% if sum of last three currentD > .8*3*slope of inhale (see below)
% elseif currentD/averagePrevPositiveD < 10% ignore
% else count-- (or count = 0 ?)
% 
% if valve close count = -3 (skip three increases)
% if change in pwm or valve then count = -c*deltaPWM*deltaValveAngle (skip proportional to change)
% 
% if count == 3 then assume exhaling, close valve and skip next several counts
% 
% Also slope of exhale is close to slope of inhale, so when threshold of inhale is reached calculate average of last couple derivates and use it to comparing to upper slope.
%					  

function flags = calcflags(pressure,d,threshold)
flags = zeros(size(d));
freezeCount = 3;% assumes 40ms sample rate
state = 'inhale';
lastDt = 5;
for idx = 3:size(d,1)
    %don't allow quick state transitions
    if freezeCount > 0
        freezeCount = freezeCount - 1;
    else
        if strcmp(state,'inhale')
            if (sum(d(idx-2:idx)) > -lastDt*3) || ...
                    (pressure(idx) > threshold && ...
                    all(pressure([idx - 2 idx - 1]) < threshold))
                flags(idx) = 40;
                freezeCount = 3;
                state = 'exhale';
            end
            
        else
            if( ... % if threshold crossing or
                    (pressure(idx) < threshold && ...
                    pressure(idx - 1) > threshold) || ...
                    ... % or if negative pressure and decreasing past couple
                    ... % of samples
                    (pressure(idx)<0) && all(d(idx-2:idx)<0)) %  d(idx) < 0)
                lastDt = d(idx);
                flags(idx) = -40;
                freezeCount = 3;
                state = 'inhale';
            end
            
        end
    end
end
