data = load('slowbreathing.log');
openflag = data(:,2) < 9;
pressure = data(:,3);
%threshold = 40;
%TODO check how delays change for now assume constant (calculate delay from
%second degree derivative (should be first minium after switching valve)
openingStartDelay = 0; %in samples
openingEndDelay = 2; 
closeingStartDelay = 2;
closeingEndDelay = 2;
%normpressure = pressure - threshold*openflag;
%linear extrapolate next point after delay that (might try cubic interpolation later)
%subtract from actual value to normalize (or change threshold accordingly
%instead)
%Could also do median filter on first derivative to remove oultiers

%Also take advantange that pwm theshold is constant over time

%Another way is to first do fast switching with a known frequency and then
%fo a bandpass around that frequency to keep calc the threshold
for idx = 2:size(data,1)
    switch state
        case 'open'
            if ~openflag(idx)
                state = 'closingStart';
                delayCounter = closingStartDelay;
            end
        case 'closed'
        case 'openingStart'
        case 'openingEnd'
        case 'closingStart'
            if delaycounter == 0
                state = 'closingEnd';
                delaycounter = closingEndCounter;
                threshold = 0;
                thresholdPerSample = 0;
            else
                delayCounter = delayCounter - 1;
            end
        case 'closingEnd'
            if delaycounter == 0
                state = 'closed';
            else
               %calculate the next pressure assuming d(idx) == d(idx-1)
               %that is the rate of pressure change would have been
               %constant if not for valve closing
               % P = Pprev - deltaPprev
               if(1)
                   %here assume that slope of pressure is constant across
                   %transition
                   estPressure = 2*pressure(idx-1)-(pressure(idx-2));
                   thresholdPerSample =  pressure(idx)- estPressure;
                   threshold = threshold + thresholdPerSample;
                   pressure(idx) = estPressure;
               else
                   %here assuming that threshold due to pwm increase is
                   %constant across samples during transition
                   if(threshold)
                   else
                       %First sample
                       estPressure = 2*pressure(idx-1)-(pressure(idx-2));
                       thresholdPerSample =  pressure(idx)- estPressure;
                       threshold = threshold + thresholdPerSample;
                       pressure(idx) = estPressure;
                   end
               end
            end
    end
    if openflag(idx-1) ~= openflag(idx)
        %transition happened
        
    end
end