%track minimum while inhaling and flag when above min by a threshold
% for exhaling open when below 0
%data = load('fastslownobreathing.log');
%data = load('fastbreathing.log');
data = load('slowbreathing.log');
pressure = data(:,3);
state = 'openValve';
exhThreshold = 10;
inThreshold = 65;
delayToTrackMin = 4;
flags = zeros(size(pressure));
minPressure = pressure(1);
for idx = 2:size(pressure,1)
    switch state
        case 'openValve' %inhaling
            if(pressure(idx) < minPressure)
                minPressure = pressure(idx);
            elseif((pressure(idx) - minPressure) > exhThreshold)
                flags(idx) = 40;
                %close valve
                state = 'closeValve';
            end
        case 'closeValve' %exhaling
            if(pressure(idx) < inThreshold && ...
                    (pressure(idx) < pressure(idx-2))) %under threshold and going down
                state = 'openingValve';
                flags(idx) = -40;
                %open Valve: pressure would go up
                delayCounter = delayToTrackMin;
            end
        case 'openingValve'
            %wait few samples before tracking minimum
            %also waiting in open is useful in case of spurious pressures
            if delayCounter == 0
                state = 'openValve';
                minPressure = pressure(idx);
            else
                delayCounter = delayCounter - 1;
            end
    end
end

plot([data(:,[2 3]) flags],'.-');