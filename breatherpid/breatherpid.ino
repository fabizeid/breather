#include "SDP.h"
#include "Wire.h"
#include <Servo.h>

#define MASize 5 //moving average size
#define ExhaleThreshold 20 //pressure in Pascal
#define InhaleThreshold 0
#define IDLE 5 //pwm val
#define MAXPOWER 255
#define MAXPOT 1024
SDP_Controller SDP800; //pressure sensor
Servo myservo;  // create servo object to control a servo

int pwmPin = 11; //9;
int servoPin = 9;
int potPin = 0; 
int loopcounter;
double potVal,servoAngle;
double currentDP, pwmVal,targetDP,error,errorOld,errorD,Kp,Kd,Ki,movingAvg,prePWM;
double DPHistory[MASize];
unsigned int circIdx;

enum BreathingStates {INHALE,
		      EXHALE};
enum BreathingStates currentState;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(pwmPin,OUTPUT);
  //Change Timer 1 clock (pins 9 (OC1A) and 10 (OC1B))
  //https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM
  //p 173 of manual: CS12 CS11 CS10: 011 /64, 100 /256, 101 /1024
  //set prescale divider for timer 1 to 1024 (101)
  //TCCR1B |= _BV(CS12)|_BV(CS10); TCCR1B &= ~_BV(CS11);
  
  //Needed to use Timer Control 2 (pins 11 and 3) since the servo library disables pwm for 
  // Timer control 1 (p202 of manual)
  //set prescale divider for timer 2 to 256 (110)
  TCCR2B |= _BV(CS22)|_BV(CS21);TCCR2B &= ~_BV(CS20);
  
   
  myservo.attach(servoPin);
  Wire.begin();
  SDP800.begin();
  SDP800.startContinuousMeasurement(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_AVERAGING_TILL_READ);
  delay(1000);
  targetDP = 20; //Pascal
  error = 0;
  errorOld = 0;
  pwmVal = IDLE; //start with slow motor
  Kp = .88;//256;//30;
  Kd = 6;//600;
  Ki = 0;
  memset(DPHistory, 0, MASize * sizeof(double));
  circIdx = 0;
  movingAvg = 0;
  currentState = INHALE;
  loopcounter = 0;
}

//void loop() {}
void loop(){
  potVal = analogRead(potPin);
  targetDP = potVal/10;
  //Kd = potVal/10;
  currentDP = SDP800.getDiffPressure();
  
  switch (currentState){
  case INHALE:
    movingAvg = movingAvg + currentDP - DPHistory[circIdx];
    DPHistory[circIdx] = currentDP;
    circIdx = (circIdx + 1) % MASize;
    if (false){
   // if( movingAvg > (targetDP + ExhaleThreshold)*MASize){
      memset(DPHistory, 0, MASize * sizeof(double));
      circIdx = 0;
      movingAvg = 0;
      //Idle the blower
      pwmVal = IDLE;
      currentState = EXHALE;
    } else {
      //Perform PD controller around target pressure
      pwmVal = pidPwm(currentDP);
    }

    
    break;
  case EXHALE:
    if (currentDP < InhaleThreshold){
      errorOld = 0;//resetPID();
      pwmVal = MAXPOWER;
      currentState = INHALE;
    }
    break;
  }

// servoAngle = map(potVal, 0, 1023, 50, 75);//limits 
 
 pwmVal = potVal/4;
 //pwmVal = 230;
 
 loopcounter = (loopcounter+1)%6;
 if (loopcounter > 3)
    //pwmVal = MAXPOWER/8;// /8;
    servoAngle = 89;
 else
    //pwmVal = 0;
   servoAngle = 106;
 

  myservo.write(servoAngle);   
  analogWrite(pwmPin,pwmVal);
  //Serial.print(Kd);
  //Serial.print(' ');
  Serial.print(pwmVal/10);
  Serial.print(' ');
  Serial.print(servoAngle/5);
  Serial.print(' ');
  //Serial.print(movingAvg/MASize);
  //Serial.print(' ');
  //Serial.print(errorD);
  //Serial.print(' ');
  Serial.println(currentDP);
  delay(40);
  
}

double pidPwm(double currentDP) {

  error = targetDP - currentDP;
  errorD = error - errorOld;
  errorOld = error;
  
  pwmVal = pwmVal + error*Kp + errorD*Kd;
  prePWM = pwmVal;
  if(pwmVal > MAXPOWER)
    pwmVal = MAXPOWER;
  else if (pwmVal < 10)
    pwmVal = IDLE;
    
 return pwmVal; 
}

