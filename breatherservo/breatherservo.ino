#include "SDP.h"
#include "Wire.h"
#include <Servo.h>

#define HistorySize 2
#define HistoryResolution 1
#define ExhaleSlopeThreshold 10
#define InhaleSlopeThreshold -10
#define InhaleThreshold 5
#define IDLE 5 //pwm val
#define MAXPOWER 255
#define MAXPOT 1024
#define OpenedServoAngle 89
//Don't exceed this or you will break servo arm
#define ClosedServoAngle 106
SDP_Controller SDP800; //pressure sensor
Servo myservo;  // create servo object to control a servo

int pwmPin = 11;
int servoPin = 9;
int potPin = 0;

double potVal,servoAngle;
double currentDP, DPSlope, pwmVal;
double DPHistory[HistorySize];
unsigned int circIdx;

enum BreathingStates {INHALE,
		      INHALEINIT,
		      EXHALE,
		      EXHALEINIT};
enum BreathingStates currentState;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(pwmPin,OUTPUT);
  //https://www.arduino.cc/en/Tutorial/SecretsOfArduinoPWM
  //Needed to use Timer Control 2 (pins 11 and 3) since the servo library disables pwm for
  // Timer control 1 (p202 of manual)
  //set prescale divider for timer 2 to 256 (110)
  TCCR2B |= _BV(CS22)|_BV(CS21);TCCR2B &= ~_BV(CS20);

  myservo.attach(servoPin);
  servoAngle = OpenedServoAngle;
  myservo.write(servoAngle);

  Wire.begin();
  SDP800.begin();
  SDP800.startContinuousMeasurement(SDP_TEMPCOMP_DIFFERENTIAL_PRESSURE, SDP_AVERAGING_TILL_READ);
  delay(1000);
  pwmVal = IDLE; //start with slow motor
  //memset(DPHistory, 0, HistorySize * sizeof(double));
  circIdx = 0;
  currentState = INHALEINIT;
}

//void loop() {}
void loop(){
  potVal = analogRead(potPin);
  pwmVal = potVal/4;
  currentDP = SDP800.getDiffPressure();

  switch (currentState){

  case INHALEINIT:
  Serial.println("INHALEINIT");
    // Initialize history buffer
    // Assuming circIdx was init 0 before entering state
    DPHistory[circIdx] = currentDP;
    if (circIdx++ > HistorySize-1){
      circIdx = 0;
      currentState = INHALE;
    }
    break;
  case INHALE:
  Serial.println("INHALE");
    DPSlope = currentDP - DPHistory[circIdx];
    DPHistory[circIdx] = currentDP;
    circIdx = (circIdx + 1) % HistorySize;
    if( DPSlope > ExhaleSlopeThreshold){

      //Close valve
      servoAngle = ClosedServoAngle;

      //TODO Idle the blower when good predictive model
      //is implemented. Since blower will needs time to ramp-up
      //before opening valve.

      currentState = EXHALEINIT;
    }

    //TODO if needed turn on pid while inhaling
    /*else {
      //Perform PD controller around target pressure
      pwmVal = pidPwm(currentDP);
      }*/
    break;

  case EXHALEINIT:
  Serial.println(EXHALEINIT);
    // Initialize history buffer
    // Assuming circIdx was init 0 before entering state
    DPHistory[circIdx] = currentDP;
    if (circIdx++ > HistorySize-1){
      circIdx = 0;
      currentState = EXHALE;
    }

    // if started to inhale while here
    // open valve immediately and swith to inhaleinit
    if (currentDP < InhaleThreshold){
      servoAngle = OpenedServoAngle;
      circIdx = 0;
      currentState = INHALEINIT;
    }

    break;

  case EXHALE:
  Serial.println("EXHALE");
    DPSlope = currentDP - DPHistory[circIdx];
    DPHistory[circIdx] = currentDP;
    circIdx = (circIdx + 1) % HistorySize;
    if( currentDP < InhaleThreshold ||
	DPSlope < InhaleSlopeThreshold){
      circIdx = 0;
      servoAngle = OpenedServoAngle;
      currentState = INHALEINIT;
    }
    break;
  }
  myservo.write(servoAngle);
  analogWrite(pwmPin,pwmVal);

  Serial.print(pwmVal/10);
  Serial.print(' ');
  Serial.print(servoAngle/10);
  Serial.print(' ');
  Serial.print(DPSlope/10);
  Serial.print(' ');
  Serial.println(currentDP);
  delay(40);

}

/*
double pidPwm(double currentDP) {

  error = targetDP - currentDP;
  errorD = error - errorOld;
  errorOld = error;

  pwmVal = pwmVal + error*Kp + errorD*Kd;
  prePWM = pwmVal;
  if(pwmVal > MAXPOWER)
    pwmVal = MAXPOWER;
  else if (pwmVal < 10)
    pwmVal = IDLE;

 return pwmVal;
}*/

